package com.example.matrimony.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.matrimony.R;
import com.example.matrimony.adapter.UserListAdapter;
import com.example.matrimony.database.tables.UserTbl;
import com.example.matrimony.model.UserModel;
import com.example.matrimony.util.Constant;

import java.util.ArrayList;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchCandidateActivity extends BaseActivity {

    @BindView(R.id.etUserSearch)
    EditText etUserSearch;
    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    String name;
    String inverseName;

    String fullName;
    String inverseFullName;

    UserListAdapter userListAdapter;
    ArrayList<UserModel> userList = new ArrayList<>();
    ArrayList<UserModel> tempUserList = new ArrayList<>();
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_candidate);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_search_candidate), false);
        setAdapter();
        searchUser();
        checkAndVisibleView();
    }

    void resetAdapter() {
        if (userListAdapter != null) {
            userListAdapter.notifyDataSetChanged();
        }
    }

    void searchUser() {
        etUserSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tempUserList.clear();
                if (s.toString().length() > 0) {

                    for (int i = 0; i < userList.size(); i++) {

                        s = s.toString().trim().toLowerCase();

                        name = userList.get(i).getUserName().toLowerCase() + " " +
                                userList.get(i).getUserSurname().toLowerCase();

                        inverseName = userList.get(i).getUserSurname().toLowerCase() + " " +
                                userList.get(i).getUserName().toLowerCase();

                        fullName = userList.get(i).getUserName().toLowerCase() + " " +
                                userList.get(i).getUserFatherName().toLowerCase() + " " +
                                userList.get(i).getUserSurname().toLowerCase();


                        if (userList.get(i).getUserName().toLowerCase().contains(s) ||
                                userList.get(i).getUserSurname().toLowerCase().contains(s) ||
                                userList.get(i).getUserFatherName().toLowerCase().contains(s) ||
                                name.contains(s) ||
                                inverseName.contains(s) ||
                                fullName.contains(s)) {
                            tempUserList.add(userList.get(i));
                        }
                    }
                }
                if (s.toString().length() == 0 && tempUserList.size() == 0) {
                    tempUserList.addAll(userList);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new UserTbl(this).getUserList());
        tempUserList.addAll(userList);
        userListAdapter = new UserListAdapter(this, tempUserList, new UserListAdapter.OnClickListener() {
            @Override
            public void onDeleteButtonClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(SearchCandidateActivity.this, CandidateDetailsActivity.class);
                intent.putExtra(Constant.USER_OBJECT, userList.get(position));
                startActivity(intent);
            }

            @Override
            public void onFavoriteButton(int position) {
                int lastUpdatedUserId = new UserTbl(SearchCandidateActivity.this).updateFavoriteStatus(userList.get(position).
                        getIsFavorite() == 0 ? 1 : 0 ,userList.get(position).getUserId());

                if (lastUpdatedUserId>0){
                    userList.get(position).setIsFavorite(userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                    userListAdapter.notifyItemChanged(position);
                }
            }
        });
        rcvUserList.setAdapter(userListAdapter);
    }


    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are You Sure Want To Delete");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedId = new UserTbl(this).deletUserById(userList.get(position).getUserId());

                    if (deletedId > 0) {
                        Toast.makeText(this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        tempUserList.remove(position);
                        userListAdapter.notifyItemRemoved(position);
                        userListAdapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();

                    } else {
                        Toast.makeText(this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    void checkAndVisibleView() {
        if (tempUserList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }

}

