// Generated code from Butter Knife. Do not modify!
package com.example.matrimony.activity;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.matrimony.R;
import com.google.android.material.tabs.TabLayout;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CandidateListByGenderActivity_ViewBinding implements Unbinder {
  private CandidateListByGenderActivity target;

  @UiThread
  public CandidateListByGenderActivity_ViewBinding(CandidateListByGenderActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CandidateListByGenderActivity_ViewBinding(CandidateListByGenderActivity target,
      View source) {
    this.target = target;

    target.tlGenders = Utils.findRequiredViewAsType(source, R.id.tlGenders, "field 'tlGenders'", TabLayout.class);
    target.vpUserList = Utils.findRequiredViewAsType(source, R.id.vpUserList, "field 'vpUserList'", ViewPager.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CandidateListByGenderActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tlGenders = null;
    target.vpUserList = null;
  }
}
