// Generated code from Butter Knife. Do not modify!
package com.example.matrimony.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.matrimony.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class UserListAdapter$UserHolder_ViewBinding implements Unbinder {
  private UserListAdapter.UserHolder target;

  @UiThread
  public UserListAdapter$UserHolder_ViewBinding(UserListAdapter.UserHolder target, View source) {
    this.target = target;

    target.tvFullName = Utils.findRequiredViewAsType(source, R.id.tvFullName, "field 'tvFullName'", TextView.class);
    target.tvLanguageCity = Utils.findRequiredViewAsType(source, R.id.tvLanguage_City, "field 'tvLanguageCity'", TextView.class);
    target.ivDeleteUser = Utils.findRequiredViewAsType(source, R.id.ivDeleteUser, "field 'ivDeleteUser'", ImageView.class);
    target.ivFavoriteUser = Utils.findRequiredViewAsType(source, R.id.ivFavoriteUser, "field 'ivFavoriteUser'", ImageView.class);
    target.tvAge = Utils.findRequiredViewAsType(source, R.id.tvAge, "field 'tvAge'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    UserListAdapter.UserHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvFullName = null;
    target.tvLanguageCity = null;
    target.ivDeleteUser = null;
    target.ivFavoriteUser = null;
    target.tvAge = null;
  }
}
